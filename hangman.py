import random


def random_word():
    """
    Returns a random word from the database.txt file.
    """
    with open(r"C:\Users\Lenovo\Documents\Python Codes\database.txt") as f:
        fc = f.read().split("\n")
        return random.choice(fc)


def hangman_word(word: str):
    """
    Generates a hangman word with random "_" in between.
    """
    length = len(word)
    word_list = list(word)

    new_word = ""

    for i in range(int(length / 2)):
        r = random.randint(0, length - 1)

        word_list[r] = "_"

    for i in word_list:
        new_word += i + ""

    return new_word, word


def launch_game():
    """
    Launches the hangman game.
    """
    asked = []
    for _ in range(10):
        while True:
            hangword, original_word = hangman_word(random_word())
            if original_word in asked:
                continue
            else:
                asked.append(original_word)
                break

        print(hangword, original_word)


if __name__ == "__main__":
    launch_game()

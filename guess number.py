import random


def guess_number():
    print("Welcome to this game called Guess the Number.")
    print("The number is between 1 and 100 and is an integer.")

    chances = 7
    print("You will get total 7 attempts to guess the number.")

    n = random.randint(1, 100)
    while chances > 0:
        guess = int(input("Guess the number: "))

        if guess == n:
            print("Congrats you won the game!")
            print(f"{chances} Chances left")
            quit()

        elif guess > n:
            print("Decrease your number")
            chances -= 1
            print(f"{chances} chances left")

        elif guess < n:
            print("Increase your number")
            chances -= 1
            print(f"{chances} chances left")

        else:
            print("Invalid input!")

        if chances == 0:
            print(f"The answer was {n}")


if __name__ == "__main__":
    guess_number()

"""
script to make computer play against computer.

built for testing purposes
"""

import os
import random
import sys
from concurrent.futures import ProcessPoolExecutor, wait


from matplotlib import pyplot as plt

from tic_tac_toe import Cell, Difficulty, TicTacToe


class HiddenPrint:
    """
    Disables all print statements by pointing sys.stdout to os.devnull.
    """

    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, "w")

    def __exit__(self, *a):
        sys.stdout.close()
        sys.stdout = self._original_stdout


class CVCBase(TicTacToe):
    """
    Provides a base for CVC tictactoe tests.
    """

    def __init__(self) -> None:
        super().__init__(Difficulty.HARD, 3)

    def fill_computer_cell_easy(self, marker: Cell) -> tuple[int, int]:
        row, col = super()._fill_computer_cell_easy()
        # overwrite cell because we need to set the given marker (default is computer)
        # and hence not using set_mark_by_coordinates
        # because it will raise overwrite error
        # same for below two functions
        self.board[row][col] = marker
        return (row, col)

    def fill_computer_cell_medium(self, marker: Cell) -> tuple[int, int]:
        row, col = super()._fill_computer_cell_medium()
        self.board[row][col] = marker
        return (row, col)

    def fill_computer_cell_hard(
        self, marker: Cell, use_minimax: bool, maximizing_player: Cell = None
    ) -> tuple[int, int]:
        if maximizing_player:
            row, col = super()._fill_computer_cell_hard(use_minimax, maximizing_player)
        else:
            row, col = super()._fill_computer_cell_hard(use_minimax)
        self.board[row][col] = marker
        return (row, col)

    def play(self) -> str:
        raise NotImplementedError("play method not implemented")


class HardVSMedium(CVCBase):
    def __init__(self) -> None:
        super().__init__()

    def play(self) -> str:
        starter = "hard" if random.randint(1, 10) % 2 == 0 else "medium"

        marker_map = {"computer": "hard", "player": "medium"}

        if starter == "hard":
            self.fill_computer_cell_hard(Cell.COMPUTER, False)

        while True:
            # self.display_board()

            self.fill_computer_cell_medium(Cell.PLAYER)
            finished, result = self.game_outcome()
            if finished:
                if not result:
                    return "draw"
                return marker_map[result.winner]

            self.fill_computer_cell_hard(Cell.COMPUTER, False)
            finished, result = self.game_outcome()
            if finished:
                if not result:
                    return "draw"
                return marker_map[result.winner]


class MinimaxVSCustom(CVCBase):
    def __init__(self) -> None:
        super().__init__()

    def play(self) -> str:
        starter = "hard" if random.randint(1, 10) % 2 == 0 else "minimax"

        marker_map = {"computer": "hard", "player": "minimax"}
        print("starter:", starter)
        if starter == "hard":
            self.fill_computer_cell_hard(Cell.COMPUTER, False)

        while True:
            self.display_board()

            self.fill_computer_cell_hard(Cell.PLAYER, True, Cell.PLAYER)
            finished, result = self.game_outcome()
            if finished:
                if not result:
                    return "draw"
                return marker_map[result.winner]

            self.fill_computer_cell_hard(Cell.COMPUTER, False)
            finished, result = self.game_outcome()
            if finished:
                if not result:
                    return "draw"
                return marker_map[result.winner]


def test(i):
    print("Running test {}...".format(i))
    with HiddenPrint():
        t2 = MinimaxVSCustom()
        result = t2.play()
    return result


if __name__ == "__main__":
    # with HiddenPrint():
    #     data = []
    #     for _ in range(100):
    #         t1 = HardVSMedium()
    #         data.append(t1.play())

    #     labels = "draw hard medium".split(" ")
    #     counts = [data.count("draw"), data.count("hard"), data.count("medium")]
    #     plt.pie(counts, labels=labels, autopct="%1.1f%%")
    #     plt.show()

    data = []
    with ProcessPoolExecutor() as pool:
        futures = [pool.submit(test, i) for i in range(1, 11)]
        done, _ = wait(futures)
        for future in done:
            data.append(future.result())

    labels = "draw hard minimax".split(" ")
    plottable = {label: data.count(label) for label in labels if data.count(label) != 0}

    plt.pie(plottable.values(), labels=plottable.keys(), autopct="%1.1f%%")
    plt.show()
